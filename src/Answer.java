import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // TODO!!! Solutions to small problems 
      //   that do not need an independent method!
    
      // conversion double -> String

      String doubleToString = Double.toString(Math.PI);
      System.out.println(doubleToString);

      String s1 = String.valueOf(Math.PI);
      System.out.println(s1);


      // conversion String -> int

      int täisarv = Integer.valueOf("678");
      System.out.println(täisarv);


      // "hh:mm:ss"

      Calendar cal = Calendar.getInstance();
      int tund = cal.get(Calendar.HOUR);
      int minut = cal.get(Calendar.MINUTE);
      int sekund = cal.get(Calendar.SECOND);

      System.out.println("hh:mm:ss --> " + tund + ":" + minut + ":" + sekund);


      // cos 45 deg

      double cos45deg = Math.cos(45);
      System.out.println(cos45deg);


      // table of square roots

      double squareRoot = Math.sqrt(5);
      System.out.println(squareRoot);

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
      long t0 = System.nanoTime();

      try {
         Thread.sleep(3000);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      long t1 = System.nanoTime();
      System.out.println("Paus oli: " + t1);


      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (new Integer (generaator.nextInt(1000)));
      }

      // minimal element

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs
      HashMap<String, String> ained = new HashMap<>();
      ained.put("i231", "Algoritmid");
      ained.put("i200", "Java");
      Iterator<String> it = ained.keySet().iterator();
      while (it.hasNext()) {
         String aine = it.next();
         System.out.println(aine);
      }




      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {

      return new StringTokenizer(text).countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      StringBuilder sb = new StringBuilder();
      for (int i=0; i<s.length(); i++) {
         char ch = s.charAt(i);
         if (Character.isUpperCase(ch)) {
            sb.append(Character.toLowerCase(ch));
         } else if (Character.isLowerCase(ch)) {
            sb.append(Character.toUpperCase(ch));
         } else {
            sb.append(ch);
         }
      }

      return sb.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
         Collections.reverse(list);
   }
}
